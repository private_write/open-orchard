using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
public class deckmanager : MonoBehaviour
{     
    // Start is called before the first frame update
    void Start()
    {
				// we need to reduce the deck size by randomly
				// deleting 5 cards at the start so we have 9
				GameObject[] cards;
				cards = GameObject.FindGameObjectsWithTag("card");
				// randomize the order and delete the first 5
				var rnd = new System.Random();
        var randomized = cards.OrderBy(item => rnd.Next());
				for (int x = 1; x <= 5 ; x++) {
						var card = randomized.ElementAt(x);
						Destroy(card);
				}
				// refresh our list of cards
				cards = GameObject.FindGameObjectsWithTag("card");
				// move two cards to the two card slots
				cards[0].transform.position = new Vector2(9,-3);
				cards[1].transform.position = new Vector2(6,-3);
				cards[2].transform.position = new Vector2(0,0);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

public static class ClassExtension
{
		public static List<GameObject> GetAllChilds(this GameObject Go)
		{
				List<GameObject> list = new List<GameObject>();
				for (int i = 0; i< Go.transform.childCount; i++)
				{
						list.Add(Go.transform.GetChild(i).gameObject);
				}
				return list;
		}
}
