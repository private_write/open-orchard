# Open Orchard

A single-player unity clone of the nine card "tile laying" solitaire game: Orchard by Mark Tuck.

## Game Requirements:

- Main menu screen
- Start game button
- Choose card back option (?)
- Description of rules
- Imported card visuals
- Flip animation at some point

## Assets Used:

- "[LPC] Fruit Trees" by bluecarrot16, Joshua Taylor, and cynicmusic. Commissioned by castelonia. CC-BY-SA 3.0 / GPL 3.0
- [Pixel Card Assets](https://opengameart.org/content/pixel-card-assets)

## Related links:

- [Orchard on boardgamegeek.com](https://boardgamegeek.com/boardgame/245487/orchard-9-card-solitaire-game)
- [Orchard Unboxing, Setup, and Solo Play Through](https://www.youtube.com/watch?v=iwzA_s9gTnQ)


Apache 2.0 License - Tessa Hall
